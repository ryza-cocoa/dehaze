#!/usr/bin/env python3.5
# -*- coding:utf-8 -*-

import optparse
import cv2
import numpy as np
import sys

def create_dehaze_options():
    usage = "usage: %prog <options>"
    parser = optparse.OptionParser(prog='dehaze', usage=usage)
    parser.add_option('-i', '--image', type='string', dest='image', help='Image to dehaze')
    parser.add_option('-o', '--output', type='string', dest='output', help='Path to save the output image')
    return parser

def ordfilt2(I, orderth, blocksize):
    height, width = I.shape
    bl, bw = blocksize
    

def calVSMap(I, r):
    height, width, _ = I.shape
    hsvI = cv2.cvtColor(I, cv2.COLOR_BGR2HSV)
    s = hsvI[:,:,1];
    v = hsvI[:,:,2];
    sigma = 0.041337;
    sigmaMat = np.random.normal(0, sigma, [height, width]);
    output = 0.121779 + 0.959710*v  - 0.780245*s + sigmaMat;
    outputPixel = output;
    output = ordfilt2(output, 1, (r, r));
    return (outputPixel, output)

def guidedfilter(I, p, r, eps):
    '''Guided Filter'''
    height, width = I.shape
    m_I = cv2.boxFilter(I, -1, (r, r))
    m_p = cv2.boxFilter(p, -1, (r, r))
    m_Ip = cv2.boxFilter(I * p, -1, (r, r))
    cov_Ip = m_Ip - m_I * m_p
    m_II = cv2.boxFilter(I * I, -1, (r, r))
    var_I = m_II - m_I * m_I
    a = cov_Ip / (var_I + eps)
    b = m_p - a * m_I
    m_a = cv2.boxFilter(a, -1, (r, r))
    m_b = cv2.boxFilter(b, -1, (r, r))
    return m_a * I + m_b

def deHaze(m, r = 15, beta = 1.0, eps = 0.001, gimfiltR = 60):
    dR, dP = calVSMap(m, r)
    refineDR = guidedfilter(dR, m / 255.0, gimfiltR, eps)

if __name__ == '__main__':
    parser = create_dehaze_options()
    (option, args) = parser.parse_args()
    
    if option.image == None or option.output == None:
        print(parser.format_help())
        sys.exit(1)

    # 读取需要去雾的图片
    haze = cv2.imread(option.image)
    
    # 应用暗通道先验去雾算法
    dehazed = deHaze(haze)
    
    # 输出图像
    cv2.imwrite(option.output, dehazed)
